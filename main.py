from flask import Flask, escape, request
from c5s4ys_assignment1 import *
import urllib.request
import json
from flask_pymongo import PyMongo


app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/PYTHONCLASS2020"
mongo = PyMongo(app)

@app.route('/')
def greet():
    return 'Hello, dear user!'

@app.route('/echo')
def echo():
    data = request.args.get("echostring")
    print(data)
    return data

@app.route('/calc', methods=['GET', 'POST'])
def get_ab_sum_and_div():
    result = {}
    if request.method == 'POST':
        return get_sum_and_div_json(int(request.json['a']), int(request.json['b']))
    else:
        return get_sum_and_div_json(int(request.args.get("a")), int(request.args.get("b")))

@app.route('/fasta', methods=['POST'])
def control_fasta():
    payload = request.json
    with urllib.request.urlopen(payload['source']) as f1:
        text = f1.read().decode('utf-8')
    with open('protein.fasta', 'w') as f2:
        f2.truncate(0)
        for line in text:
            f2.write(line)
    proteins = load_fasta('protein.fasta')
    print(payload['arg'])
    if payload['function'] != "num_with_motif":
        return {"retval": "bad function", "num_proteins":len(proteins)}
    return {"retval": len(find_protein_with_motif(proteins, payload['arg'])), "num_proteins":len(proteins)}


@app.route('/mongo/names', methods=['POST'])
def test_mongo_name():
    payload = request.json
    names = mongo.db.main.find({"iteration": payload['iteration']})
    result = list(map(lambda x: x['name'], names)) 
    return json.dumps(result)

@app.route('/mongo/clear', methods=['POST', 'DELETE', 'GET'])
def mongo_clear():
    mongo.db.c5s4ys.delete_many({})
    return "200"

@app.route('/mongo/create_user', methods=['POST'])
def mongo_create_user():
    payload = request.json
    mongo.db.c5s4ys.insert_one(payload)
    return "200"

@app.route('/mongo/auth/names', methods=['POST'])
def mongo_names_auth():
    payload = request.json
    users = mongo.db.c5s4ys.find({"username": payload['username']})
    filtered_users = []
    for user in users:
        if user['password'] == payload['password']:
            filtered_users.append(user)  
    if users.count() == 0:
        return {"status": "bad username", "list_of_names": []}
    if len(filtered_users) == 0:
        return {"status": "bad password", "list_of_names": []}
    filtered_users = list(map(lambda x: x['username'], filtered_users)) 
    return {"list_of_names": filtered_users, "status": "ok"}


def get_sum_and_div_json(a, b):
    result = {}
    result['sum'] = a + b
    try:
        result['div'] = a/b
    except ZeroDivisionError:
        return "Cant devide by 0"
    return result

    

